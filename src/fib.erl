-module(fib).

-ifdef(TEST).
-include_lib("proper/include/proper.hrl").
-include_lib("eunit/include/eunit.hrl").
-endif.

-export([fib/1]).

fib(0) ->
    1;
fib(1) ->
    1;
fib(N) when N > 1 ->
    fib(N, 2, fib(0), fib(1)).

fib(N, N, PrePrev, Prev) ->
    PrePrev + Prev;
fib(N, M, PrePrev, Prev) ->
    fib(N, M + 1, Prev, PrePrev + Prev).

-ifdef(TEST).

fib_test_() -> [
    {"fib(0) =:= 1", ?_assert(fib(0) =:= 1)},
    {"fib(1) =:= 1", ?_assert(fib(1) =:= 1)},
    {"fib(2) =:= 2", ?_assert(fib(2) =:= 2)},
    {"fib(3) =:= 3", ?_assert(fib(3) =:= 3)},
    {"fib(4) =:= 5", ?_assert(fib(4) =:= 5)},
    {"fib(5) =:= 8", ?_assert(fib(5) =:= 8)},
    {"fib(-1) throws exception",
        ?_assertException(error, function_clause, fib(-1))},
    {"fib(31) =:= 2178309", ?_assert(fib(31) =:= 2178309)},
    {"forall X. fib(X) + fib(X + 1) =:= fib(X + 2)",
        ?_assert(proper:quickcheck(prop_fib()))}].

prop_fib() ->
    ?FORALL(X, pos_integer(), fib(X) + fib(X + 1) =:= fib(X + 2)).

-endif.
